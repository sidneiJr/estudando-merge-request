const subtraction = require('../subtraction');

test('1 - 2 = -1', () => {
    expect(subtraction('1', '2')).toBe(-1)
})